<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# deep 0.3.13

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_deep/develop?logo=python)](
    https://gitlab.com/ae-group/ae_deep)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_deep/release0.3.12?logo=python)](
    https://gitlab.com/ae-group/ae_deep/-/tree/release0.3.12)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_deep)](
    https://pypi.org/project/ae-deep/#history)

>ae_deep module 0.3.13.

[![Coverage](https://ae-group.gitlab.io/ae_deep/coverage.svg)](
    https://ae-group.gitlab.io/ae_deep/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_deep/mypy.svg)](
    https://ae-group.gitlab.io/ae_deep/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_deep/pylint.svg)](
    https://ae-group.gitlab.io/ae_deep/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_deep)](
    https://gitlab.com/ae-group/ae_deep/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_deep)](
    https://gitlab.com/ae-group/ae_deep/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_deep)](
    https://gitlab.com/ae-group/ae_deep/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_deep)](
    https://pypi.org/project/ae-deep/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_deep)](
    https://gitlab.com/ae-group/ae_deep/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_deep)](
    https://libraries.io/pypi/ae-deep)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_deep)](
    https://pypi.org/project/ae-deep/#files)


## installation


execute the following command to install the
ae.deep module
in the currently active virtual environment:
 
```shell script
pip install ae-deep
```

if you want to contribute to this portion then first fork
[the ae_deep repository at GitLab](
https://gitlab.com/ae-group/ae_deep "ae.deep code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_deep):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_deep/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.deep.html
"ae_deep documentation").
